import 'dart:io';

int m = 0,x=0;
void func1(var n) {
  m = n ~/ 2;
  for (int i = 2; i <= m; i++) {
    if (n % i == 0) {
      print('$n is not a prime number');
      x=1;
      break;
    }
  }
  if(x==0){
    print('$n is a prime number');
  }
}
void main() {
  int? Num;
  print("input number : ");
  Num = int.parse(stdin.readLineSync()!);
  func1(Num);
}